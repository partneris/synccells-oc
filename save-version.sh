##!/bin/bash
VERSION="0.1.0"
#
# reading configuration

configfile="save-version.config"

source $configfile

if [ "$1" == "-p" ]
then
	# creating version_packages directory if not existing
	if [ ! -d ../packages ]; then
	echo "======"
	echo "version_packages folder not found ... creating..."
	mkdir ../packages
	fi

	echo
	echo "Creating package to ../packages/${package_file_name}_testing.ocmod.zip"
	echo
	rm ../packages/${package_file_name}_testing.ocmod.zip
	cat $filelist | zip -r "../packages/${package_file_name}_testing.ocmod.zip" -@ > ../packages/make-package.log
	exit
fi

# Checking if a GIT repository is here and if it has remote to push to
remote_exists=`git remote`

if [ ! $remote_exists ]; then
  echo "git repo not found here. or git remote is not defined. exiting...."
  exit
fi



# checking that all files existing where the version change has to be done
for filename in "${file_with_version_info[@]}"
do	

if [ ! -f "$filename" ]
then
echo
echo SCRIPT HALTED!!!
echo "File where the version info is defined is not found!"
echo $directory_name/$filename
echo
echo Please check that the files listed in create_new_package.config exist!
echo Please also make sure that you create_new_package.config file lines end 
echo with semi-colon, or there is no line break at the last line
echo "exiting..."
echo
exit
fi

done

echo
echo
echo "PREVIOUS RELEASES"
echo "================="
echo
git tag -n1
echo
echo "Enter current release info - what is changed:"
echo
read release_info

#testing that new version is not empty
if [ -z "$release_info" ]
   then
   echo "Release info not supplied. Exiting..."
   exit
fi

#current_version=`git describe --tags --abbrev=0`

#input new version number and release info
echo "===================================="
echo
echo "Current version is: "$current_version
echo "Enter new version number in the form of x.x.x"
echo
read new_version

#testing that new version entered is not empty
if [ -z "$new_version" ]
   then
   echo "New version not supplied. Exiting..."
   exit
fi


for filename in "${file_with_version_info[@]}"
do

# replace the version information in the file_with_version_info

########### this is version for non-ubuntu ##########
# 	sed "s/_VERSION\([\'\"]\), [\'\"][[:digit:]]*\.[[:digit:]]*\.[[:digit:]]*[\"\']/_VERSION\1, \"$new_version\"/" $directory_name/$filename > .temporary_output_file
# 	#todo: test if changed, if not changed, means string not found... nothing to add
# 	mv .temporary_output_file $directory_name/$file_with_version_info
########### end non-ubuntu ##########

extension=${filename##*.}
extension=${extension:0:3}

# search replace for php files
if [ "$extension" == "php" ]; then
sed "s/_VERSION\([\'\"]\), [\'\"][[:digit:]]*\.[[:digit:]]*\.[[:digit:]]*[\"\']/_VERSION\1, \"$new_version\"/" -i $filename
fi

#search/replace version in vqmod files
if [ "$extension" == "xml" ]; then
sed "s/<version>.*[0-9].*\.[0-9].*\.[0-9]\+.*<\/version>/<version><![CDATA[$new_version]]><\/version>/" -i $filename

fi
if [ "$extension" == "gs" ]; then
echo "working on js"
sed -r "s/VERSION[[:space:]]*=[[:space:]]*[\'\"].+[\'\"]/VERSION = \"$new_version\"/" -i $filename

fi

done

echo $new_version: `date`: $release_info >> .releases
git add .releases

for file in $(cat $filelist); do
    git add --all "$file"
done

git commit -m "$release_info"

git pull

git push --set-upstream origin master

# get last tagged version

#testing if entered version is already existing
tags=`git tag`

if [[ $tags == *$new_version* ]]; then
    echo
    echo "new version will be THE SAME as one of previous"
    echo "backing up previous and creating new ..."
    echo
    t=`date +"%Y%m%d%H%M%S"`
    git tag $new_version.$t $new_version
    git tag -d $new_version
    if [ $remote_exists ]; then
        git push origin :refs/tags/$new_version
    fi
fi

# add the new tag to the repository
git tag $new_version
if [ $remote_exists ]; then
	git push --tags
fi

git describe --tags --long > current-version.info

# creating version_packages directory if not existing
if [ ! -d ../packages ]; then
echo "======"
echo "version_packages folder not found ... creating..."
mkdir ../packages
fi

echo
echo "Creating package to ../packages/${package_file_name}_v${new_version}${package_file_name_suffix}.zip"
echo
cat $filelist | zip -r "../packages/${package_file_name}_v${new_version}${package_file_name_suffix}.zip" -@ > ../packages/make-package.log

exit