Syncsheets second major version.


getProductMappingDialogData() should receive the following sturcture

  data = 
  {
    "languages" : [
        {
        "language_id" : 3,
        "name" : "Latviešu"
        },
        {
        "language_id" : 4,
        "name" : "Russian"
        }
    ],
    
    "attributes" : [
        {
        "attribute_id" : 1,
        "name" : "Lietots"
        }
      },
      {}
    ]
  };
  
How to delete product description (and other tables) where product_id not exist. https://docs.google.com/spreadsheets/d/1ry53gO6K3S5493s2ThqLuJLdJSTTwxVWSYpFgOIWZEs/edit#gid=439547937

DELETE FROM oc_product_description
WHERE product_id IN (
    SELECT pid FROM (
        select a.product_id AS pid from oc_product_description a left outer join oc_product b on a.product_id = b.product_id where b.product_id is NULL
    ) AS p
)

