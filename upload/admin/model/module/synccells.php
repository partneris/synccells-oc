<?php
class ModelModuleSynccells extends Model {

	public function getSettings() {
		$setting_data = array();

		$query = $this->db->query("SELECT * FROM `synccells_setting`");

		foreach ($query->rows as $result) {
			if (!$result['encoded']) {
				$setting_data[$result['key']] = $result['value'];
			} else {
				$setting_data[$result['key']] = json_decode($result['value'], true);
			}
		}

		return $setting_data;
	}
	
	public function editSetting($data) {
		$this->db->query("DELETE FROM `synccells_setting`");

		foreach ($data as $key => $value) {
			if (!is_array($value)) {
				$this->db->query("INSERT INTO `synccells_setting` SET `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
			} else {
				$this->db->query("INSERT INTO `synccells_setting` SET `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(json_encode($value, true)) . "', encoded = '1'");
			}
		}
	}

	public function install(){
		$this->db->query("CREATE TABLE IF NOT EXISTS `synccells_setting` (
		  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
		  `key` varchar(64) NOT NULL,
		  `value` text NOT NULL,
		  `encoded` tinyint(1) NOT NULL,
		  PRIMARY KEY (`setting_id`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");
	}

}
?>