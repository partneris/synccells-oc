<?php
class ModelToolSynccells extends Model {
	
	public function addAttribute($data) {
        
        $languages = $this->getLanguages();
 		
 		$this->db->query("INSERT INTO " . DB_PREFIX . "attribute SET attribute_group_id = '" . (int)$data['attribute_group_id'] . "'");

		$attribute_id = $this->db->getLastId();

		foreach ($languages as $language) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "attribute_description SET attribute_id = '" . (int)$attribute_id . "', language_id = '" . (int)$language['language_id'] . "', name = '" . $this->db->escape($data['name']) . "'");
		}

		return $attribute_id;
        
	}
	
	public function getLanguages() {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "language` ORDER BY `sort_order`");
        return $query->rows;
	}
	
	public function getProductMappingDialogData() {
        
		$data = array();
		$language_data = $this->getLanguages();
		
		$attribute_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "attribute a LEFT JOIN " . DB_PREFIX . "attribute_description ad ON (a.attribute_id = ad.attribute_id) WHERE ad.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY ad.name ASC");

		foreach ($query->rows as $result) {
			$attribute_data[] = array(
				'attribute_id' => $result['attribute_id'],
				'name' => $result['name']
			);
		}
		
		$filter_data = array();
        
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "filter_group fg LEFT JOIN " . DB_PREFIX . "filter_group_description fgd ON (fg.filter_group_id = fgd.filter_group_id) WHERE fgd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY fgd.name ASC");
		
		foreach ($query->rows as $result) {
			$filter_data[] = array(
				'filter_group_id' => $result['filter_group_id'],
				'name' => $result['name']
			);
		}
		$customer_group_data = array();
        
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_group_description WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY customer_group_id ASC");
		
		foreach ($query->rows as $result) {
			$customer_group_data[] = array(
				'customer_group_id' => $result['customer_group_id'],
				'name' => $result['name']
			);
		}
        
		$data = array(
			'languages'          => $language_data,
			'attributes'         => $attribute_data,
			'filters'            => $filter_data,
			'customer_groups'    => $customer_group_data
		);

		return $data;
	}

	public function getProduct( $index, $indexField ) {
		
		$query = $this->db->query("SELECT p.*, m.name AS manufacturer FROM `" . DB_PREFIX . "product` p 
			LEFT JOIN `" . DB_PREFIX . "manufacturer` m ON (p.manufacturer_id = m.manufacturer_id)
			WHERE p." . $indexField . " = '" . $this->db->escape(htmlspecialchars($index, ENT_COMPAT, "UTF-8")) . "'");

		return $query->row;
	}
	
	public function getProductsByIndexField( $index_field, $filters = array() ) {
		
		$sql = "SELECT " . $this->db->escape($index_field) . " FROM `" . DB_PREFIX . "product` ";
		
		foreach ( $filters as $filter ) {
			if ( $filter['field'] == 'category' ) {
				
			}
		}
		
		$query = $this->db->query( $sql );
		
		return $query->rows;
	}
	
	public function getProductDescription( $product_id, $language_id ) {
		
		$query = $this->db->query("
			SELECT * FROM `" . DB_PREFIX . "product_description` 
			WHERE product_id = '" . (int)$product_id . "'
			AND language_id = '" . (int)$language_id . "'");
		
		return $query->row;
	}

	public function getProductAttribute( $product_id, $language_id, $attribute_id ) {
		
		$query = $this->db->query("
			SELECT * FROM `" . DB_PREFIX . "product_attribute` 
			WHERE product_id = '" . (int)$product_id . "'
			AND language_id = '" . (int)$language_id . "'
			AND attribute_id = '" . (int)$attribute_id . "'");
		
		return $query->row;
	}

	public function getProductStores( $product_id ) {
	
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_to_store` WHERE  product_id = '" . (int)$product_id . "'");
		
		$stores = array();
		
		foreach ( $query->rows as $store ) {
			$stores[] = $store['store_id'];
		}
		
		return $stores;
	}
	
	public function getManufacturerByName( $name ) {
		
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "manufacturer` WHERE name = '" . $this->db->escape($name) . "'");
		return $query->row;
	}
	
	public function getProductCategories( $product_id ) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_to_category` WHERE  product_id = '" . (int)$product_id . "'");
		return $query->rows;
	}
	
	public function getCategoryPath( $category_id, $language_id, $separator = '&nbsp;&gt;&nbsp;' ) {
	
		$query = $this->db->query("
			SELECT GROUP_CONCAT(cd1.name ORDER BY level SEPARATOR '" . $this->db->escape($separator) . "') AS 'path'
			FROM `" . DB_PREFIX . "category_path` cp 
			LEFT JOIN `" . DB_PREFIX . "category_description` cd1 ON (cp.path_id = cd1.category_id ) 
			WHERE cp.category_id = '" . (int)$category_id . "' AND cd1.language_id = '" . (int)$language_id . "' 
			GROUP BY cp.category_id
		");

		return $query->row['path'];
	}
	
	public function getCategoryPaths( $language_id, $separator = '>' ) {
	
		$query = $this->db->query("
			SELECT cp.category_id, GROUP_CONCAT(cd1.name ORDER BY level SEPARATOR '" . $this->db->escape($separator) . "') AS 'path'
			FROM `" . DB_PREFIX . "category_path` cp 
			LEFT JOIN `" . DB_PREFIX . "category_description` cd1 ON (cp.path_id = cd1.category_id ) 
			WHERE cd1.language_id = '" . (int)$language_id . "' 
			GROUP BY cp.category_id
		");
		
		$paths = array();
		
		foreach ( $query->rows as $row ) {
			$paths[html_entity_decode($row['path'], ENT_QUOTES, 'UTF-8')] = $row['category_id'];
		}
		
		return $paths;
	}
	
	public function getProductImages( $product_id ) {
        
		$query = $this->db->query("
			SELECT * 
			FROM `" . DB_PREFIX . "product_image` pi 
			WHERE product_id = '" . (int)$product_id . "' 
			ORDER BY pi.sort_order ASC
		");
        
        return $query->rows;
	}
	
	public function getSEOKeyword( $product_id ) {
        
		$query = $this->db->query("
			SELECT keyword 
			FROM `" . DB_PREFIX . "url_alias` 
			WHERE query = 'product_id=" . (int)$product_id . "'
		");
        
		if ($query->num_rows) {
			$keyword = $query->row['keyword'];
		} else {
			$keyword = '';
		}
		
        return $keyword;
	}
	
	public function updateProductToCategory($product_id, $categories = array()) {
		//first delete all that are not in $categories array
		$query = $this->db->query("
			DELETE FROM `" . DB_PREFIX . "product_to_category` 
			WHERE product_id = '" . (int)$product_id . "'
			AND category_id NOT IN (" . join(", ", $categories) .  ")
		");
		foreach ( $categories  as $category_id ) {
			$query = $this->db->query("
				INSERT INTO `" . DB_PREFIX . "product_to_category` 
				SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'
				ON DUPLICATE KEY UPDATE product_id = product_id
			");
		}
	}
	
	public function editProductSpecials($product_id, $product_specials) {
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
		
		foreach ($product_specials as $product_special) {
			if ($product_special['price']) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_special['customer_group_id'] . "', priority = '" . (int)$product_special['priority'] . "', price = '" . (float)$product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
			}
		}
	}
	
	public function editProductSeoKeyword($product_id, $product_seo_keyword) {
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id . "'");
		
			if (!empty($product_seo_keyword)) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape($product_seo_keyword) . "'");
			}
	}
	
	public function getSetting($key = '') {
		if ($key) {
			$query = $this->db->query("SELECT * FROM `synccells_setting` WHERE `key` = '" . $this->db->escape($key) . "'");
			
			if ($query->num_rows) {
				if (!$query->row['encoded']) {
					return $query->row['value'];			
				} else {
					return json_decode($query->row['value']);
				}
			}
		} else {
			$setting_data = array();
			
			$query = $this->db->query("SELECT * FROM `synccells_setting`");
			
			foreach ($query->rows as $result) {
				if (!$result['encoded']) {
					$setting_data[$result['key']] = $result['value'];
				} else {
					$setting_data[$result['key']] = json_decode($result['value']);
				}
			}

			return $setting_data;
		}
	}
	
	public function addProductDefaults($index, $indexField, $data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "product SET " . $indexField . " = '" . $this->db->escape($index) . "', quantity = '" . (int)$data['synccells_quantity'] . "', minimum = '" . (int)$data['synccells_minimum'] . "', subtract = '" . (int)$data['synccells_subtract'] . "', stock_status_id = '" . (int)$data['synccells_stock_status_id'] . "', shipping = '" . (int)$data['synccells_shipping'] . "', weight_class_id = '" . (int)$data['synccells_weight_class_id'] . "', length_class_id = '" . (int)$data['synccells_length_class_id'] . "', status = '" . (int)$data['synccells_product_status'] . "', tax_class_id = '" . (int)$data['synccells_tax_class_id'] . "', date_added = NOW()");

		$product_id = $this->db->getLastId();

		$data['product_description'] = array();
		
        $language = $this->db->query("SELECT * FROM `" . DB_PREFIX . "language`");

	    foreach ($language->rows as $lang) {
			$data['product_description'][$lang['language_id']] = $index;
	    }
		
		foreach ($data['product_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value) . "'");
		}

		if (isset($data['synccells_product_store'])) {
			foreach ($data['synccells_product_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		//=== addProductDefaults insert custom ===
		
		//=== end addProductDefaults insert custom ===
		
		//$this->cache->delete('product');

		return $product_id;
	}
	
	public function editProduct($product_operations, $product_id) {
        
        $pairs = array();
        
        foreach ( $product_operations as $operation ) {
            $pairs[] = "`" . $operation['field_name'] . "` = '" . $operation['field_value'] . "'";
        }
        
        $sql = "UPDATE " . DB_PREFIX . "product SET ";
        $sql .= join(", ", $pairs);
        $sql .= ", date_modified = NOW()";
        $sql .= " WHERE product_id = '" . (int)$product_id . "'";
        $this->db->query($sql);
	}
	
	public function editProductDescription($product_description_operations, $product_id) {
        
        foreach ( $product_description_operations as $language_id => $operation ) {
            
            $pairs = array();
            
            foreach ($operation as $field) {
                $pairs[] = "`" . $field['field_name'] . "` = '" . $field['field_value'] . "'";
            }
            
            $sql = "UPDATE " . DB_PREFIX . "product_description SET ";
            $sql .= join(", ", $pairs);
            $sql .= " WHERE product_id = '" . (int)$product_id . "' AND language_id = '" . (int)$language_id . "'";
            $this->db->query($sql);
        }
	}
	
	public function editProductAttribute($product_id, $attribute_id, $language_id, $value) {
        
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE 
            product_id = '" . (int)$product_id . "' AND
            attribute_id = '" . (int)$attribute_id . "' AND
            language_id = '" . (int)$language_id . "'"
        );
        if ($value != "") {
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET 
                product_id = '" . (int)$product_id . "',
                attribute_id = '" . (int)$attribute_id . "',
                language_id = '" . (int)$language_id . "',
                text = '" . $this->db->escape($value) . "'"
            );
        }
	}
	
    public function editProductImages($product_id, $images) {
        
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");
        
        $sort_order = 0;
        
        foreach ( $images as $image ) {
                
            $sort_order++;
            
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($image) . "', sort_order = '" . (int)$sort_order . "'");
            
        }
    }

	public function getOptions($language_id) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "option` o LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE od.language_id = '" . (int)$language_id . "' ORDER BY od.name ASC";

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getOptionValues($option_id, $language_id) {
		$option_value_data = array();

		$option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_value ov LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE ov.option_id = '" . (int)$option_id . "' AND ovd.language_id = '" . (int)$language_id . "' ORDER BY ov.sort_order, ovd.name");

		foreach ($option_value_query->rows as $option_value) {
			$option_value_data[] = array(
				'option_value_id' => $option_value['option_value_id'],
				'name'            => $option_value['name'],
				'image'           => $option_value['image'],
				'sort_order'      => $option_value['sort_order']
			);
		}

		return $option_value_data;
	}

	public function addOption($option_name, $option_type) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "option` SET type = '" . $this->db->escape($option_type) . "', sort_order = '0'");

		$option_id = $this->db->getLastId();

        $language = $this->db->query("SELECT * FROM `" . DB_PREFIX . "language`");

	    foreach ($language->rows as $lang) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "option_description SET option_id = '" . (int)$option_id . "', language_id = '" . (int)$lang['language_id'] . "', name = '" . $this->db->escape($option_name) . "'");
		}

		return $option_id;
	}
	
	public function addOptionValue($option_id, $option_value_name) {
		$option_value_id = '';

		if (!empty($option_value_name)) {
			$language = $this->db->query("SELECT * FROM `" . DB_PREFIX . "language`");
			
			$this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET option_id = '" . (int)$option_id . "', image = '', sort_order = '0'");

			$option_value_id = $this->db->getLastId();

			foreach ($language->rows as $lang) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "option_value_description SET option_value_id = '" . (int)$option_value_id . "', language_id = '" . (int)$lang['language_id'] . "', option_id = '" . (int)$option_id . "', name = '" . $this->db->escape($option_value_name) . "'");
			}
		}

		return $option_value_id;
	}

	public function editProductOptions($product_id, $data) {

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");

		foreach ($data as $product_option) {
			if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
				if (isset($product_option['product_option_value'])) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (int)$product_option['required'] . "'");

					$product_option_id = $this->db->getLastId();

					foreach ($product_option['product_option_value'] as $product_option_value) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$product_option_value['option_value_id'] . "', quantity = '" . (int)$product_option_value['quantity'] . "', subtract = '" . (int)$product_option_value['subtract'] . "', price = '" . (float)$product_option_value['price'] . "', price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', points = '" . (int)$product_option_value['points'] . "', points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', weight = '" . (float)$product_option_value['weight'] . "', weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "'");
					}
				}
			} else {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', value = '" . $this->db->escape($product_option['value']) . "', required = '" . (int)$product_option['required'] . "'");
			}
		}
	}
	public function getProductOptions($product_id, $language_id) {
		$product_option_data = array();

		$product_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$language_id . "' ORDER BY o.sort_order");

		foreach ($product_option_query->rows as $product_option) {
			$product_option_value_data = array();

			$product_option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_id = '" . (int)$product_id . "' AND pov.product_option_id = '" . (int)$product_option['product_option_id'] . "' AND ovd.language_id = '" . (int)$language_id . "' ORDER BY ov.sort_order");

			foreach ($product_option_value_query->rows as $product_option_value) {
				$product_option_value_data[] = array(
					'product_option_value_id' => $product_option_value['product_option_value_id'],
					'option_value_id'         => $product_option_value['option_value_id'],
					'name'                    => $product_option_value['name'],
					'image'                   => $product_option_value['image'],
					'quantity'                => $product_option_value['quantity'],
					'subtract'                => $product_option_value['subtract'],
					'price'                   => $product_option_value['price'],
					'price_prefix'            => $product_option_value['price_prefix'],
					'weight'                  => $product_option_value['weight'],
					'weight_prefix'           => $product_option_value['weight_prefix'],
					'points'                  => $product_option_value['points']
				);
			}

			$product_option_data[] = array(
				'product_option_id'    => $product_option['product_option_id'],
				'product_option_value' => $product_option_value_data,
				'option_id'            => $product_option['option_id'],
				'name'                 => $product_option['name'],
				'type'                 => $product_option['type'],
				'value'                => $product_option['value'],
				'required'             => $product_option['required']
			);
		}

		return $product_option_data;
	}
	
	public function getFilters($language_id) {
		$sql = "SELECT *, (SELECT name FROM " . DB_PREFIX . "filter_group_description fgd WHERE f.filter_group_id = fgd.filter_group_id AND fgd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS `group` FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_description fd ON (f.filter_id = fd.filter_id) WHERE fd.language_id = '" . (int)$language_id . "'";

		$sql .= " ORDER BY f.sort_order ASC";

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getProductFilters( $product_id, $language_id, $filter_group_id ) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_filter` pf LEFT JOIN " . DB_PREFIX . "filter_description fd ON (fd.filter_id = pf.filter_id) WHERE pf.product_id = '" . (int)$product_id . "' AND fd.filter_group_id = '" . (int)$filter_group_id . "' AND fd.language_id = '" . (int)$language_id . "' ORDER BY fd.filter_id ASC");
        
        return $query->rows;
	}
	
	public function editFilter($new_filter_name, $filter_group_id) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "filter SET filter_group_id = '" . (int)$filter_group_id . "', sort_order = '0'");

		$filter_id = $this->db->getLastId();

        $language = $this->db->query("SELECT * FROM `" . DB_PREFIX . "language`");

	    foreach ($language->rows as $lang) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "filter_description SET filter_id = '" . (int)$filter_id . "', language_id = '" . (int)$lang['language_id'] . "', filter_group_id = '" . (int)$filter_group_id . "', name = '" . $this->db->escape($new_filter_name) . "'");
		}

		return $filter_id;
	}

	public function editProductFilters($product_id, $filter_group_id, $data) {
		// get product categories
		$product_category_data = array();
		$category_filter_data = array();

		$query_cat = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query_cat->rows as $result) {
			$product_category_data[] = $result['category_id'];
		}
		// end of get product categories

		// get product filters by filter_group_id
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_filter` pf LEFT JOIN " . DB_PREFIX . "filter f ON (f.filter_id = pf.filter_id) WHERE pf.product_id = '" . (int)$product_id . "' AND f.filter_group_id = '" . (int)$filter_group_id . "'");
		
		$filters = array();
		
		foreach ( $query->rows as $filter ) {
			if (in_array($filter['filter_id'], $data)) {
				$filters[] = $filter['filter_id'];
			} else {
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "' AND filter_id = '" . (int)$filter['filter_id'] . "'");			
			}
		}
		
		// get category filters by filter_group_id
		foreach ($product_category_data as $product_category_d) {
			$query_cat_filters = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_filter` cf LEFT JOIN " . DB_PREFIX . "filter f ON (f.filter_id = cf.filter_id) WHERE cf.category_id = '" . (int)$product_category_d . "' AND f.filter_group_id = '" . (int)$filter_group_id . "'");
			foreach ($query_cat_filters->rows as $result_cat_filters) {
				$category_filter_data[] = array(
					'category_id' => $result_cat_filters['category_id'],
					'filter_id'   => $result_cat_filters['filter_id']
				);
			}
		}
//$this->log->write($category_filter_data);
		
		foreach ($data as $product_filter) {
			if (!in_array($product_filter, $filters)) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET product_id = '" . (int)$product_id . "', filter_id = '" . (int)$product_filter . "'");			
			}
			
			$is_cat_filter = 0;
			foreach ($product_category_data as $product_cat_d) {
				foreach ($category_filter_data as $category_filter_d) {
					if (($category_filter_d['filter_id'] == $product_filter) && ($category_filter_d['category_id'] == $product_cat_d)) {
						$is_cat_filter = 1;
					}
				}
				if (!$is_cat_filter) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "category_filter SET category_id = '" . (int)$product_cat_d . "', filter_id = '" . (int)$product_filter . "'");	
				}
			}
			
		}
	}

    public function editProductStores($product_id, $stores) {
        
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");
        
        foreach ( $stores as $store ) {
                
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . $this->db->escape($store) . "'");
            
        }
    }
    
    public function getProductRelated( $product_id, $index_field ) {
        
        $query = $this->db->query("SELECT p.". $this->db->escape( $index_field ) . " FROM `" . DB_PREFIX . "product_related` pr LEFT JOIN " . DB_PREFIX . "product p ON (pr.related_id = p.product_id) WHERE pr.product_id = '" . (int)$product_id . "' ORDER BY pr.related_id ASC");
        
        return $query->rows;
    
    }

    public function editProductRelated( $product_id, $related_product_ids ) {
    
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");
        
        foreach ( $related_product_ids as $related_product_id ) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_product_id . "'");
        }
    }
    
	public function getProductSpecials( $product_id ) {	
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_special` WHERE  product_id = '" . (int)$product_id . "'");
		
		$specials = array();
		
		foreach ( $query->rows as $special ) {
			$specials[] = array($special['customer_group_id'], $special['priority'], $special['price'], $special['date_start'], $special['date_end']);
		}
		
		if (!empty($specials)) return json_encode($specials); else return $specials;
	}

	public function getProductDiscounts( $product_id ) {	
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_discount` WHERE  product_id = '" . (int)$product_id . "'");
		
		$discounts = array();
		
		foreach ( $query->rows as $discount ) {
			$discounts[] = array($discount['customer_group_id'], $discount['quantity'], $discount['priority'], $discount['price'], $discount['date_start'], $discount['date_end']);
		}
		
		if (!empty($discounts)) return json_encode($discounts); else return $discounts;
	}

	public function editProductDiscounts($product_id, $product_discounts) {
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "'");
		
		foreach ($product_discounts as $product_discount) {
			if ($product_discount['price']) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', quantity = '" . (int)$product_discount['quantity'] . "', priority = '" . (int)$product_discount['priority'] . "', price = '" . (float)$product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
			}
		}
	}
	
	//=== insert custom functions ===
	//=== end insert custom functions ===
	
}