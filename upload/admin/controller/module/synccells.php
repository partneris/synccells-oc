<?php
define('SYNCCELLS_VERSION', "1.3.5");
class ControllerModuleSynccells extends Controller {
	
	private $error = array(); 
	
	public function index() {   
		$this->load->language('module/synccells');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('module/synccells');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_module_synccells->editSetting($this->request->post);		

			$this->session->data['success'] = $this->language->get('text_success');
						
			if (version_compare(VERSION, '2.3', '>')) {
				$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
			}
		}

		$text_strings = array(
				'heading_title',
				'text_edit',
				'text_none',
				'text_yes',
				'text_no',
				'text_enabled',
				'text_disabled',
				'text_default',
				'text_module_version',
				'text_choose',
				'text_select',
				'text_radio',
				'text_checkbox',
				'text_image',
				'text_input',
				'text_text',
				'text_textarea',
				'text_file',
				'text_date',
				'text_time',
				'text_datetime',
				'tab_general',
				'tab_product_values',
				'tab_category_values',
				'tab_option_values',
				'entry_secret',
				'entry_tax_class',
				'entry_quantity',
				'entry_minimum_quantity',
				'entry_subtract_stock',
				'entry_out_of_stock_status',
				'entry_requires_shipping',
				'entry_length_class',
				'entry_weight_class',
				'entry_store',
				'entry_product_status',
				'entry_categories_top',
				'entry_option_name',
				'entry_option_type',
				'entry_option_required',
				'entry_option_quantity',
				'entry_option_subtract',
				'entry_option_price',
				'entry_option_points',
				'entry_option_weight',
				'help_minimum',
				'help_stock_status',
				'help_top',
				'button_save',
				'button_cancel'
		);
		
		foreach ($text_strings as $text) {
			$data[$text] = $this->language->get($text);
		}
		
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/synccells', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$data['action'] = $this->url->link('module/synccells', 'token=' . $this->session->data['token'], 'SSL');
		
		if (version_compare(VERSION, '2.3', '>')) {
			$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'], 'SSL');
		} else {
			$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		}

		$data['token'] = $this->session->data['token'];

		$synccells_settings = $this->model_module_synccells->getSettings();

		if (isset($this->request->post['synccells_secret'])) {
			$data['synccells_secret'] = $this->request->post['synccells_secret'];
		} else if ($synccells_settings && isset($synccells_settings['synccells_secret'])) {
			$data['synccells_secret'] = $synccells_settings['synccells_secret'];
		} else {
			$data['synccells_secret'] = '';
		}

		if (isset($this->request->post['synccells_tax_class_id'])) {
			$data['synccells_tax_class_id'] = $this->request->post['synccells_tax_class_id'];
		} else if ($synccells_settings && isset($synccells_settings['synccells_tax_class_id'])) {
			$data['synccells_tax_class_id'] = $synccells_settings['synccells_tax_class_id'];
		} else {
			$data['synccells_tax_class_id'] = '';
		}

		if (isset($this->request->post['synccells_quantity'])) {
			$data['synccells_quantity'] = $this->request->post['synccells_quantity'];
		} else if ($synccells_settings && isset($synccells_settings['synccells_quantity'])) {
			$data['synccells_quantity'] = $synccells_settings['synccells_quantity'];
		} else {
			$data['synccells_quantity'] = '';
		}

		if (isset($this->request->post['synccells_minimum'])) {
			$data['synccells_minimum'] = $this->request->post['synccells_minimum'];
		} else if ($synccells_settings && isset($synccells_settings['synccells_minimum'])) {
			$data['synccells_minimum'] = $synccells_settings['synccells_minimum'];
		} else {
			$data['synccells_minimum'] = '';
		}

		if (isset($this->request->post['synccells_subtract'])) {
			$data['synccells_subtract'] = $this->request->post['synccells_subtract'];
		} else if ($synccells_settings && isset($synccells_settings['synccells_subtract'])) {
			$data['synccells_subtract'] = $synccells_settings['synccells_subtract'];
		} else {
			$data['synccells_subtract'] = '';
		}

		if (isset($this->request->post['synccells_stock_status_id'])) {
			$data['synccells_stock_status_id'] = $this->request->post['synccells_stock_status_id'];
		} else if ($synccells_settings && isset($synccells_settings['synccells_stock_status_id'])) {
			$data['synccells_stock_status_id'] = $synccells_settings['synccells_stock_status_id'];
		} else {
			$data['synccells_stock_status_id'] = '';
		}

		if (isset($this->request->post['synccells_shipping'])) {
			$data['synccells_shipping'] = $this->request->post['synccells_shipping'];
		} else if ($synccells_settings && isset($synccells_settings['synccells_shipping'])) {
			$data['synccells_shipping'] = $synccells_settings['synccells_shipping'];
		} else {
			$data['synccells_shipping'] = '';
		}

		if (isset($this->request->post['synccells_length_class_id'])) {
			$data['synccells_length_class_id'] = $this->request->post['synccells_length_class_id'];
		} else if ($synccells_settings && isset($synccells_settings['synccells_length_class_id'])) {
			$data['synccells_length_class_id'] = $synccells_settings['synccells_length_class_id'];
		} else {
			$data['synccells_length_class_id'] = '';
		}

		if (isset($this->request->post['synccells_weight_class_id'])) {
			$data['synccells_weight_class_id'] = $this->request->post['synccells_weight_class_id'];
		} else if ($synccells_settings && isset($synccells_settings['synccells_weight_class_id'])) {
			$data['synccells_weight_class_id'] = $synccells_settings['synccells_weight_class_id'];
		} else {
			$data['synccells_weight_class_id'] = '';
		}

		if (isset($this->request->post['synccells_product_store'])) {
			$data['synccells_product_store'] = $this->request->post['synccells_product_store'];
		} else if ($synccells_settings && isset($synccells_settings['synccells_product_store'])) {
			$data['synccells_product_store'] = $synccells_settings['synccells_product_store'];
		} else {
			$data['synccells_product_store'] = array();
		}

		if (isset($this->request->post['synccells_product_status'])) {
			$data['synccells_product_status'] = $this->request->post['synccells_product_status'];
		} else if ($synccells_settings && isset($synccells_settings['synccells_product_status'])) {
			$data['synccells_product_status'] = $synccells_settings['synccells_product_status'];
		} else {
			$data['synccells_product_status'] = '';
		}

		if (isset($this->request->post['synccells_top'])) {
			$data['synccells_top'] = $this->request->post['synccells_top'];
		} else if ($synccells_settings && isset($synccells_settings['synccells_top'])) {
			$data['synccells_top'] = $synccells_settings['synccells_top'];
		} else {
			$data['synccells_top'] = '';
		}

		if (isset($this->request->post['synccells_option_description'])) {
			$data['synccells_option_description'] = $this->request->post['synccells_option_description'];
		} else if ($synccells_settings && isset($synccells_settings['synccells_option_description'])) {
			$data['synccells_option_description'] = $synccells_settings['synccells_option_description'];
		} else {
			$data['synccells_option_description'] = array();
		}

		if (isset($this->request->post['synccells_option_type'])) {
			$data['synccells_option_type'] = $this->request->post['synccells_option_type'];
		} else if ($synccells_settings && isset($synccells_settings['synccells_option_type'])) {
			$data['synccells_option_type'] = $synccells_settings['synccells_option_type'];
		} else {
			$data['synccells_option_type'] = '';
		}

		if (isset($this->request->post['synccells_option_required'])) {
			$data['synccells_option_required'] = $this->request->post['synccells_option_required'];
		} else if ($synccells_settings && isset($synccells_settings['synccells_option_required'])) {
			$data['synccells_option_required'] = $synccells_settings['synccells_option_required'];
		} else {
			$data['synccells_option_required'] = '';
		}

		if (isset($this->request->post['synccells_option_quantity'])) {
			$data['synccells_option_quantity'] = $this->request->post['synccells_option_quantity'];
		} else if ($synccells_settings && isset($synccells_settings['synccells_option_quantity'])) {
			$data['synccells_option_quantity'] = $synccells_settings['synccells_option_quantity'];
		} else {
			$data['synccells_option_quantity'] = '';
		}

		if (isset($this->request->post['synccells_option_subtract'])) {
			$data['synccells_option_subtract'] = $this->request->post['synccells_option_subtract'];
		} else if ($synccells_settings && isset($synccells_settings['synccells_option_subtract'])) {
			$data['synccells_option_subtract'] = $synccells_settings['synccells_option_subtract'];
		} else {
			$data['synccells_option_subtract'] = '';
		}

		if (isset($this->request->post['synccells_option_price_prefix'])) {
			$data['synccells_option_price_prefix'] = $this->request->post['synccells_option_price_prefix'];
		} else if ($synccells_settings && isset($synccells_settings['synccells_option_price_prefix'])) {
			$data['synccells_option_price_prefix'] = $synccells_settings['synccells_option_price_prefix'];
		} else {
			$data['synccells_option_price_prefix'] = '';
		}

		if (isset($this->request->post['synccells_option_price'])) {
			$data['synccells_option_price'] = $this->request->post['synccells_option_price'];
		} else if ($synccells_settings && isset($synccells_settings['synccells_option_price'])) {
			$data['synccells_option_price'] = $synccells_settings['synccells_option_price'];
		} else {
			$data['synccells_option_price'] = '';
		}

		if (isset($this->request->post['synccells_option_points_prefix'])) {
			$data['synccells_option_points_prefix'] = $this->request->post['synccells_option_points_prefix'];
		} else if ($synccells_settings && isset($synccells_settings['synccells_option_points_prefix'])) {
			$data['synccells_option_points_prefix'] = $synccells_settings['synccells_option_points_prefix'];
		} else {
			$data['synccells_option_points_prefix'] = '';
		}

		if (isset($this->request->post['synccells_option_points'])) {
			$data['synccells_option_points'] = $this->request->post['synccells_option_points'];
		} else if ($synccells_settings && isset($synccells_settings['synccells_option_points'])) {
			$data['synccells_option_points'] = $synccells_settings['synccells_option_points'];
		} else {
			$data['synccells_option_points'] = '';
		}

		if (isset($this->request->post['synccells_option_weight_prefix'])) {
			$data['synccells_option_weight_prefix'] = $this->request->post['synccells_option_weight_prefix'];
		} else if ($synccells_settings && isset($synccells_settings['synccells_option_weight_prefix'])) {
			$data['synccells_option_weight_prefix'] = $synccells_settings['synccells_option_weight_prefix'];
		} else {
			$data['synccells_option_weight_prefix'] = '';
		}

		if (isset($this->request->post['synccells_option_weight'])) {
			$data['synccells_option_weight'] = $this->request->post['synccells_option_weight'];
		} else if ($synccells_settings && isset($synccells_settings['synccells_option_weight'])) {
			$data['synccells_option_weight'] = $synccells_settings['synccells_option_weight'];
		} else {
			$data['synccells_option_weight'] = '';
		}

		$this->load->model('localisation/tax_class');

		$data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		
		$this->load->model('localisation/stock_status');

		$data['stock_statuses'] = $this->model_localisation_stock_status->getStockStatuses();
		
		$this->load->model('localisation/weight_class');

		$data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();
		
		$this->load->model('localisation/length_class');

		$data['length_classes'] = $this->model_localisation_length_class->getLengthClasses();
		
		$this->load->model('setting/store');

		$data['stores'] = $this->model_setting_store->getStores();
		
		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/synccells.tpl', $data));
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/synccells')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		return !$this->error;
	}
	
	public function install() {
		if (!$this->user->hasPermission('modify', 'module/synccells')) {
			if (version_compare(VERSION, '2.3', '>')) {
				$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
			}
		} else {		

			$this->load->model('module/synccells');
			
			$this->model_module_synccells->install();
		
			if (version_compare(VERSION, '2.3', '>')) {
				$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
			}
		}
	}

}
?>