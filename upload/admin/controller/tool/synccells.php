<?php
define('SYNCCELLS_VERSION', "1.3.5");
class ControllerToolSynccells extends Controller {
	
	private $language_ids;
	private $start_time; // will hold starting time
	
	public function index() {
        try {
            $this->start_time = time(); //starting counter
            //using file_get_contents('php://input');
            //because application/json is not one of the content-types that will populate $_POST:
            //http://stackoverflow.com/questions/8893574/php-php-input-vs-post 
            $json = file_get_contents('php://input');
            $payload = json_decode($json, true); //decoding as assoc array
            if (!$payload) die("Check if you have correct store url (with or without www, https:// or http://)");
            
            //$this->log->write($payload);
            
            $sheet_data			= $payload['sheetData'];
            $incoming_hash 		= $payload['hash'];
            $nonce 				= $payload['nonce'];
            $user				= $payload['user'];
            $spreadsheet_id		= $payload['spreadsheetId'];
            $action 			= $payload['action'];
            
            $this->load->model('tool/synccells');
            
            $privateKey = html_entity_decode($this->model_tool_synccells->getSetting('synccells_secret'));
            
            $calculated_hash = base64_encode(hash_hmac('sha512', $user. $spreadsheet_id . $action . $nonce, $privateKey, true));
            
            $json = '';
            
            if ( $privateKey && $calculated_hash == $incoming_hash && $this->validateNonce($nonce, $user) ) {
                
                $this->load->model('tool/synccells');
                
                //Constructing public variable $language_ids
                $languages = $this->model_tool_synccells->getLanguages();
                $this->language_ids = array();
                foreach ($languages as $lang) {
                    $this->language_ids[strtolower($lang['code'])] = $lang['language_id'];
                }
                
                //request is from valid source, continue depending on action
                switch($action) {
                    
                    case "testConnection":
                        $json = array('success' => "Connection valid");
                        break;
                    
                    case "getConnection":
                        $json = array(
                            'driver' 		=> DB_DRIVER,
                            'hostname' 		=> DB_HOSTNAME,
                            'username' 		=> DB_USERNAME,
                            'password'		=> DB_PASSWORD,
                            'database'		=> DB_DATABASE,
                            'port'			=> DB_PORT,
                            'prefix'		=> DB_PREFIX
                        );
                        break;
                    //endcase
                    
                    case "pullSelectedRange":
                        $json = $this->pullSelectedRange($sheet_data);
                        break;
                        
                    case "pushSelectedRange":
                        $json = $this->pushSelectedRange($sheet_data);
                        //throw new Exception("This is on purpose");
                        //while (true) { sleep(1); }
                        break;
                    
                    case "pushOptions":
                        $settings = $this->getSettings($user, $sheet_id);
                        $json = $settings;
                        break;
                    
                    case "getNewIds":
                        $rows = $sheet_data['rows'];
                        
                        $filter = $sheet_data['filter'];
                        $index_field = $sheet_data['indexField'];
                        
                        $this->load->model('tool/synccells');
                        
                        $ids = $this->model_tool_synccells->getProductsByIndexField($index_field, $filter);
                        
                        $result = array();
                        
                        foreach( $ids as $id ) {
                            $search = $this->heDecodeIfString($id[$index_field]);
                            if ( empty($rows) || !isset( $rows[$search] ) ) {
                                $result[] = $search;
                            }
                        }
                        
                        $json = $result;
                        break;
                    
                    case "getImageList":
                        $json = $this->getImageList($sheet_data['startingPath']);
                        break;
                        
                    
                    case "getProductMappingDialogData":
                        $json = $this->model_tool_synccells->getProductMappingDialogData();
                        break;
                        
                    case "addAttribute":
                    
                        $json = $this->model_tool_synccells->addAttribute($sheet_data);
                        
                        break; 
                //endcase
                }
            } else { //end if request valid
                $json = array( "error" => "Not authorized. Check if secret is the same on both ends. Alternatively check if SyncCells Module is installed on Opencart.");
            }
        } catch (Exception $e) {
            $this->log->write("SyncCells: " .  $e->getMessage());
            $json = array("error" => "SyncCells: " .  $e->getMessage());
        }
        
        if (isset($this->request->server['HTTP_ORIGIN'])) {
            $this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
            $this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
            $this->response->addHeader('Access-Control-Max-Age: 1000');
            $this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
        }
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}
    
    private function timeSublimit($k = 0.9) {
        $limit = ini_get('max_execution_time'); // changes even when you set_time_limit()
        $sub_limit = round($limit * $k);
        if($sub_limit === 0) {
            $sub_limit = INF;
        }
        return $sub_limit;
    }	
	private function checkTime() {
        $time_spent = time() - $this->start_time;
        if($time_spent >= $this->timeSublimit()) {
            throw new Exception('Time limit '. ini_get('max_execution_time'). ' seconds reached. You need to increase PHP max_execution_time');
        }
	
	}
	private function getSettings($user, $sheet_id) {
		
		return "sample settings";
	}
	
	private function validateNonce($nonce, $user) {
	//reads the database, each user has stored value with last used nonce. new nonce must be bigger than that
		return true;
	}
	
	private function heDecodeIfString($value) {
		return gettype($value) == "string" ? html_entity_decode($value, ENT_QUOTES, 'UTF-8') : $value;
	}
	
	private function pullSelectedRange($sheet_data) {
		
		$rows         = $sheet_data['rows'];
		$header       = $sheet_data['header'];
		$index_field   = $sheet_data['indexField'];
		$pull_options  = $sheet_data['pullOptions'];
        $options_language_id = $sheet_data['optionsLanguageId'];
		
		$columns = array();
		$ids = array();
        $options = array();
		
		//Preparing some variables
		foreach ( $header as &$head ) {
			$head['full_field_name'] = $head['field'];
			$parts = explode(".", $head['field']);
			
			if ( !isset($parts[1]) ) {
				return  array("error"=> "Field name does not contain dot (.). Check column '" . $head['column_name'] . "'");
			} else {
				
				$head['table'] = $parts[0];
				$head['field'] = $parts[1];
				
			}
		}
		unset($head);
		
		foreach ( $rows as $index ) { 
			
			//test for empty index. If index value not exists, do nothing
			if ( $index == '' ) continue;
			
			$tables = array(); //will contain data retrieved from db, for caching purposes
			
			$tables['product'] = $this->model_tool_synccells->getProduct($index, $index_field);
			
			if ( empty($tables['product']) ) continue; //such product id does not exist MUST MARK AS DELETED
			
			$ids[] = $index;
			$product_id = $tables['product']['product_id'];
			
			if ( $pull_options ) {
                $options[] = $this->model_tool_synccells->getProductOptions($product_id, $options_language_id);
            }
            
			//Iterating columns of one product
			foreach ( $header as $head ) {
				
				////// Pull Product Base fields ///////////
				if ( preg_match("/^product\..+/i", $head['full_field_name']) ) {
					//field is one of fields from product table, where no alterations needed
					
					$columns[$head['column_name']][] = $this->heDecodeIfString($tables['product'][$head['field']]);
				
				////// Pull Product Description ////////////
				} else if ( preg_match("/^product_description\..+/i", $head['full_field_name']) ) {
					
					//return error if language_id is not set
					if ( !isset($head['language_id']) ) 
						return array( "error" => "language_id for column " . $head['column_name'] . " is not set");
					
					if ( !isset($tables['product_description'][$head['language_id']]) ) {
						$tables['product_description'][$head['language_id']] = $this->model_tool_synccells->getProductDescription($product_id, $head['language_id']);
					}
					
					if ( empty($tables['product_description'][$head['language_id']]) ) {
						$columns[$head['column_name']][] = '';
					} else {
						$columns[$head['column_name']][] = $this->heDecodeIfString($tables['product_description'][$head['language_id']][$head['field']]);
					}
				
				////// Pull Product Attributes ////////////
				} else if ( preg_match("/^product_attribute\..+/i", $head['full_field_name']) ) {
					
					//return error if language_id is not set
					if ( !isset($head['language_id']) ) 
						return array( "error" => "language_id for column " . $head['column_name'] . " is not set");
					
					if ( !isset($tables['product_attribute'][$head['language_id']][$head['attribute_id']]) ) {
						$tables['product_attribute'][$head['language_id']][$head['attribute_id']] = $this->model_tool_synccells->getProductAttribute($product_id, $head['language_id'], $head['attribute_id']);
					}
					
					if ( empty($tables['product_attribute'][$head['language_id']][$head['attribute_id']]) ) {
						$columns[$head['column_name']][] = '';
					} else {
						$columns[$head['column_name']][] = $this->heDecodeIfString($tables['product_attribute'][$head['language_id']][$head['attribute_id']][$head['field']]);
					}
				
				/////// Pull Categories //////////
				} else if ( preg_match("/^category.path/i", $head['full_field_name']) ) {
					
					//return error if language_id is not set
					if ( !isset($head['language_id']) ) 
						return array( "error" => "language_id for column " . $head['column_name'] . " is not set");
					
					//return error if multipleCategoriesSeparator is not set
					if ( !isset($head['multipleCategoriesSeparator']) ) 
						return array( "error" => "multipleCategoriesSeparator for column " . $head['column_name'] . " is not set");
					
					$category_ids = $this->model_tool_synccells->getProductCategories($product_id);
					
					$categories = array();
					
					foreach ( $category_ids as $category ) {
						$categories[] = $this->model_tool_synccells->getCategoryPath($category['category_id'], $head['language_id']);
					}
					
					$categories = join( $head['multipleCategoriesSeparator'], $categories );
					
					$columns[$head['column_name']][] = html_entity_decode( $categories, ENT_QUOTES, 'UTF-8' );
				
				//////// Pull SEO Keywords ///////////////
				} else if ( $head['full_field_name'] == 'url_alias.keyword' ) {
                    
                    $seo_keyword = $this->model_tool_synccells->getSEOKeyword($product_id);
                    
                    if ( $seo_keyword ) {
                        
                        $columns[$head['column_name']][] = html_entity_decode( $seo_keyword, ENT_QUOTES, 'UTF-8' );
                        
                    } else {
                        $columns[$head['column_name']][] = "";
                    }
				//////// Pull Additional Images ///////////////
				} else if ( $head['full_field_name'] == 'product_image.image' ) {
                    
                    $images = $this->model_tool_synccells->getProductImages($product_id);
                    
                    if ( $images ) {
                        
                        $imagelist = array();
                    
                        foreach ( $images as $image ) {
                            $imagelist[] = $image['image'];
                        }
                        
                        $columns[$head['column_name']][] = join($head['listSeparator'], $imagelist);
                        
                    } else {
                        $columns[$head['column_name']][] = "";
                    }
                    
                    
				//////// Pull Filters ///////////////
				} else if ( $head['full_field_name'] == 'product_filter.name' ) {
                    
                    $filters = $this->model_tool_synccells->getProductFilters($product_id, $head['language_id'], $head['filter_group_id']);
                    $filters_separator = isset($head['multiple_filters_separator']) ? $head['multiple_filters_separator'] : ", ";
                    
                    if ( $filters ) {
                        
                        $filter_list = array();
                    
                        foreach ( $filters as $filter ) {
                            $filter_list[] = $filter['name'];
                        }
                        
                        $columns[$head['column_name']][] = join($filters_separator, $filter_list);
                        
                    } else {
                        $columns[$head['column_name']][] = "";
                    }
                    
				//////// Pull Related ///////////////
				} else if ( $head['full_field_name'] == 'product_related.index' ) {
                    
                    $related = $this->model_tool_synccells->getProductRelated($product_id, $index_field);
                    $items_separator = isset($head['multiple_items_separator']) ? $head['multiple_items_separator'] : ", ";
                    
                    if ( $related ) {
                        
                        $related_list = array();
                    
                        foreach ( $related as $item ) {
                            $related_list[] = $item[$index_field];
                        }
                        
                        $columns[$head['column_name']][] = join($items_separator, $related_list);
                        
                    } else {
                        $columns[$head['column_name']][] = "";
                    }
                    
				//////// Pull Product Stores ///////////////
				} else if ( $head['full_field_name'] == 'product_to_store.store' ) {
                    
                    $items_separator = isset($head['multiple_items_separator']) ? $head['multiple_items_separator'] : ", ";
                    $stores = $this->model_tool_synccells->getProductStores($product_id);
                    
                    if ( $stores ) {
                        
                        $storelist = array();
                    
                        foreach ( $stores as $store ) {
                            $storelist[] = $store;
                        }
                        
                        $columns[$head['column_name']][] = join($items_separator, $storelist);
                        
                    } else {
                        $columns[$head['column_name']][] = "";
                    }
                    
				//////// Pull Product Special Prices ///////////////
				} else if ( $head['full_field_name'] == 'special.prices' ) {
                    
                    $special_prices = $this->model_tool_synccells->getProductSpecials($product_id);
                    
                    if (!empty($special_prices)) {
                        
//                        $specialslist = array();
                    
//                        foreach ( $special_prices as $special_price ) {
//                            $specialslist[] = $special_price;
//                        }
                        
//                        $columns[$head['column_name']][] = '[' . join(', ', $specialslist) . ']';
                        $columns[$head['column_name']][] = $special_prices;
                        
                    } else {
                        $columns[$head['column_name']][] = "";
                    }
                    
				//////// Pull Product Discounts ///////////////
				} else if ( $head['full_field_name'] == 'discount.prices' ) {
                    
                    $discounts = $this->model_tool_synccells->getProductDiscounts($product_id);
                    
                    if (!empty($discounts)) {
                        
//                        $discountslist = array();
                    
//                        foreach ( $discounts as $discount ) {
//                            $discountslist[] = $discount;
//                        }
                        
//                        $columns[$head['column_name']][] = '[' . join(', ', $discountslist) . ']';
                        $columns[$head['column_name']][] = $discounts;
                        
                    } else {
                        $columns[$head['column_name']][] = "";
                    }
                    
                    
                    
                    
                    
				}
			} //end foreach header
		} //end foreach rows
		$result['ids'] = $ids;
		$result['columns'] = $columns;
		if ( $pull_options ) {
            $result['options'] = $options;
        }
		return $result;
	}
	
	private function pushSelectedRange($sheet_data) {
		
		$rows = $sheet_data['rows'];
		$header = $sheet_data['header'];
		$index_field = $sheet_data['indexField'];
		$synccells_options = $sheet_data['options'];
		
		if (isset($sheet_data['optionFields'])) {
            $synccells_option_language = $sheet_data['optionsLanguageId'];
			$synccells_option_fields = $sheet_data['optionFields'];

			$key_option_option = '';
			$key_option_type = '';
			$key_option_required = '';
			$key_option_value = '';
			$key_option_quantity = '';
			$key_option_subtract = '';
			$key_option_price = '';
			$key_option_points = '';
			$key_option_weight = '';
					
			foreach ($synccells_option_fields as $key => $value) {
				if ($value == 'option.option') $key_option_option = $key;
				else if ($value == 'option.type') $key_option_type = $key;
				else if ($value == 'option.required') $key_option_required = $key;
				else if ($value == 'option.value') $key_option_value = $key;
				else if ($value == 'option.quantity') $key_option_quantity = $key;
				else if ($value == 'option.subtract') $key_option_subtract = $key;
				else if ($value == 'option.price') $key_option_price = $key;
				else if ($value == 'option.points') $key_option_points = $key;
				else if ($value == 'option.weight') $key_option_weight = $key;
			}
		}
		
		$columns = array();
		$ids = array();
		
		//Preparing some variables
		foreach ( $header as $col => &$head ) {
            if ( !isset($head['field']) ) 
                return  array("error"=> "Header " . ($col + 1) . " does not contain property 'field'");
			$head['full_field_name'] = $head['field'];
			$parts = explode(".", $head['field']);
			
			if ( !isset($parts[1]) ) {
				return  array("error"=> "Field name " . $head['full_field_name'] . " does not contain dot (.). Check column '" . $head['column_name'] . "'");
			} else {
				
				$head['table'] = $parts[0];
				$head['field'] = $parts[1];
				
			}
		}
		unset($head);
		
		$language_data = $this->model_tool_synccells->getLanguages();
		$languages = array();
		foreach ( $language_data as $data ) {
			$languages[] = $data['language_id'];
		}
		
		$this->load->model('catalog/manufacturer'); //for adding new manufacturers
		$this->load->model('catalog/category'); //for adding new categories
		
		$this->load->model('module/synccells');
		
		$synccells_default_settings = $this->model_module_synccells->getSettings();

        if ( !empty($synccells_options) && (isset($sheet_data['optionFields'])) ) {
            // get all options and option values from opencart
            $all_options = $this->model_tool_synccells->getOptions($synccells_option_language);

            foreach ($all_options as $result) {	
            
                $option_value_data = array();
                
                if ($result['type'] == 'select' || $result['type'] == 'radio' || $result['type'] == 'checkbox' || $result['type'] == 'image') {
                    $all_option_values = $this->model_tool_synccells->getOptionValues($result['option_id'], $synccells_option_language);
                
                    foreach ($all_option_values as $option_value) {
                        $option_value_data[strip_tags(html_entity_decode($option_value['name'], ENT_QUOTES, 'UTF-8'))] = array(
                            'option_value_id' => $option_value['option_value_id'],
                            'image'           => $option_value['image']
                        );
                    }
                }
                
                $options[strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))] = array(
                    'option_id'    => $result['option_id'],
                    'type'         => $result['type'],
                    'sort_order'   => $result['sort_order'],
                    'option_values' => $option_value_data
                );
                
            }
            // end of get all options and option values from opencart
        }
        //=== push insert before row loop ===
        
        //=== end push insert before row loop ===
        
		$i = -1;
		
		foreach ( $rows as $index ) { //rowNumber is not used
            $this->checkTime();
			$i++; 
			//test for empty index value
			if ( $index == "" ) continue; // index value is blank, moving to next row
			
			$product = $this->model_tool_synccells->getProduct($index, $index_field);
			
			if ( empty($product) ) {
                //product with such index value not found in database, MUST CREATE ONE		
                $product_id = $this->model_tool_synccells->addProductDefaults($index, $index_field, $synccells_default_settings);
			} else {
			// begin update product
			
                $product_id = $product['product_id'];
			}
			$product_operations = array();
			$product_description_operations = array();
			$product_seo_keyword = '';
			$product_seo_keyword_head = 0;
			$product_attributes = array();
			$product_filters = array();
            unset($product_specials);
			unset($product_discounts);

			$product_stores = $this->model_tool_synccells->getProductStores($product_id);
			
			//=== push insert before column loop ===
			
			//=== end push insert before column loop ===
			
			//Iterating columns (fields) of one product
			foreach ( $header as $head ) {
				
				////// Push Product Base fields ///////////
				if ( preg_match("/^product\..+/i", $head['full_field_name']) ) {
					
					$value = htmlspecialchars($head['data'][$i], ENT_COMPAT, 'UTF-8');
					
					//field is one of fields from product table, where no alterations needed
					switch ( $head['field'] ) {
						case 'model' :
						case 'sku' :
						case 'upc' :
						case 'ean' :
						case 'jan' :
						case 'isbn' :
						case 'mpn' :
						case 'location' :
						case 'date_available' :
						case 'image' :
							//$product_operations[] = $head['field'] . " = '" . $this->db->escape($value) . "'";
							$product_operations[] = array(
                                'field_name' => $head['field'],
                                'field_value' => $this->db->escape($value)
                            );
							break;
						case 'stock_status_id' :
						case 'manufacturer_id' :
						case 'tax_class_id' :
						case 'weight_class_id' :
						case 'length_class_id' :
						case 'minimum' :
						case 'sort_order' :
						case 'quantity' :
						case 'viewed' :
						case 'points' :
						case 'shipping' :
						case 'subtract' :
						case 'status' :
							//$product_operations[] = $head['field'] . " = '" . (int)$value . "'";
							$product_operations[] = array(
                                'field_name' => $head['field'],
                                'field_value' => (int)$value
                            );
							break;
						case 'price' :
						case 'weight' :
						case 'length' :
						case 'width' :
						case 'height' :
							//$product_operations[] = $head['field'] . " = '" . (float)$value . "'";
							$product_operations[] = array(
                                'field_name' => $head['field'],
                                'field_value' => (float)$value
                            );
							break;
						case 'manufacturer' :
							if ($value == '') {
								$manufacturer_id = 0;
							} else {
								$manufacturer = $this->model_tool_synccells->getManufacturerByName($value);
								
								if ($manufacturer) {
									$manufacturer_id = $manufacturer['manufacturer_id'];
								} else {
									//need to create manufacturer
									
									$data = array(
										"name" => $value,
										"sort_order" => 0,
										"manufacturer_store" => $product_stores
									);
									
									$manufacturer_id = $this->model_catalog_manufacturer->addManufacturer($data);
								}
							}
							//$product_operations[] = "manufacturer_id = '" . (int)$manufacturer_id . "'";
							$product_operations[] = array(
                                'field_name' => 'manufacturer_id',
                                'field_value' => (int)$manufacturer_id
                            );
							//$product_operations variable is actioned below search for "if ( !empty($product_operations) ) {"
							break;
					}
				} //end Product Base Fields
				
				////// Push Product Description ////////////
				else if ( preg_match("/^product_description\..+/i", $head['full_field_name']) ) {
					
					if ( !isset($head['language_id']) ) 
						return array( "error" => "language_id for column " . $head['column_name'] . " is not set");
					
					$value = htmlspecialchars($head['data'][$i], ENT_COMPAT, 'UTF-8');
					
					switch ($head['field']) {
						case 'name' :
						case 'description' :
						case 'tag' :
						case 'meta_title' :
						case 'meta_description' :
						case 'meta_keyword' :
                            //$product_description_operations[$head['language_id']][] = $head['field'] . " = '" . $this->db->escape($value) . "'";
                            $product_description_operations[$head['language_id']][] = array(
                                'field_name' => $head['field'],
                                'field_value' => $this->db->escape($value),
							);
							break;
						//end case
					}
				} ////////////end Product Description
				
				///////// Push Category Paths ///////////////	
				else if ( preg_match("/^category.path/i", $head['full_field_name']) ) {
					
					$value = $head['data'][$i];
					
					//checking if laguage_id is set in column properties
					if ( !isset($head['language_id']) ) 
						return array( "error" => "language_id for column " . $head['column_name'] . " is not set");
					
					//checking if multiple categories separator is set in column properties
					if ( !isset($head['multipleCategoriesSeparator']) )
						return array( "error" => "multipleCategoriesSeparator for column " . $head['column_name'] . " is not set");
					
					//building array of category paths if not yet done
					if ( !isset($category_paths[$head['language_id']]) ) {
						$category_paths[$head['language_id']] = $this->model_tool_synccells->getCategoryPaths($head['language_id'], ">"); //must retrieve html_entity_decoded
					}
					
					$product_categories = array(); //this will store categories for product_to_category table
					
					//splitting multiple categories
					$categories = $this->trimArrayElements(explode(trim($head['multipleCategoriesSeparator']), $value));
					
					foreach ( $categories as $category ) {
						
						$parts = $this->trimArrayElements(explode(">", $category));
						
						$path = join(">", $parts);
						
						if ( isset($category_paths[$path]) ) {
						
							$category_id = $category_paths[$path];
						
						} else { //this is new category path
						
							$parent_id = 0;
							
							$search_array = array();
							
							//iterating from top level category down to more specific
							foreach ( $parts as $part ) {
								
								$search_array[] = $part;
								$search_string = join(">", $search_array);
								
								if ( !isset($category_paths[$head['language_id']][$search_string]) ) {
									//-----------this is new category, need to create one
									$data = array();
									$data['sort_order'] = 0; // will be on the very top
									$data['status'] = 1; //1=enabled;
									$data['column'] = 1; //in how many columns the category dropdown to be displayed
									
									$name = htmlspecialchars($part, ENT_COMPAT, 'UTF-8');
									
									foreach ( $languages as $language_id ) {
										$lang_array = array(
												'name' => $name,
												'meta_title' => $name,
												'meta_keyword' => '',
												'meta_description' => '',
												'description' => ''
										);
										$data['category_description'][$language_id] = $lang_array;
									}
									
									$data['category_store'] = $product_stores;
									
									$data['keyword'] = '';
									
									$data['parent_id'] = $parent_id;
									
									$category_id = $this->model_catalog_category->addCategory($data);
									
									$category_paths[$head['language_id']][$search_string] = $category_id;
									//-----------end create category
								} else {
									$category_id = $category_paths[$head['language_id']][$search_string];
								}
								$parent_id = $category_id;
							} //end foreach parts
						} //end this is new category path
						//now we have the category_id of the path, creating product to category statements
						$product_categories[] = $category_id;
					} //end foreach category
					//saving product_to_category
					$this->model_tool_synccells->updateProductToCategory($product_id, $product_categories);
				} //end  category path 
				
				///////// Push Product Filters ///////////////	
				else if ( $head['full_field_name'] == 'product_filter.name' ) {
                    
                    if ( !isset($head['language_id']) ) 
						return array( "error" => "language_id for column " . $head['column_name'] . " is not set");
					
                    if ( !isset($head['filter_group_id']) ) 
						return array( "error" => "filter_group_id for column " . $head['column_name'] . " is not set");

                    // get this group filters from opencart
                    $all_filters = array();
                    $all_filters = $this->model_tool_synccells->getFilters($head['language_id']);

                    foreach ($all_filters as $result) {
                        $filters[strip_tags(html_entity_decode($result['group'], ENT_QUOTES, 'UTF-8')).'>'.strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))] = array(
                            'filter_id'        => $result['filter_id'],
                            'language_id'      => $result['language_id'],
                            'filter_group_id'  => $result['filter_group_id'],
                            'sort_order'       => $result['sort_order'],
                            'group'            => $result['group']
                        );
                    }
                    // end of get this group filters from opencart

					$value = htmlspecialchars($head['data'][$i], ENT_COMPAT, 'UTF-8');
					
                    $product_filters = array();
                    
                    // get filters from synccells
                    $filters_separator = isset($head['multiple_filters_separator']) ? trim($head['multiple_filters_separator']) : ",";
                    
                    $parts = $this->trimArrayElements(explode($filters_separator, $value));
                    
                    foreach ($parts as $search_filter) {
                        if (!empty($search_filter)) {
                            if (isset($filters[$head['column_name'].'>'.$search_filter])) {
                                // if filter exist in opencart
                                
                                $new_filter_id = $filters[$head['column_name'].'>'.$search_filter]['filter_id'];
                                
                                if (!in_array($new_filter_id, $product_filters)) {
                                    $product_filters[] = $new_filter_id;
                                }
                                
                            } else {
                                // create new filter
                                $new_filter_id = $this->model_tool_synccells->editFilter($search_filter, $head['filter_group_id']);
                                
                                if (!in_array($new_filter_id, $product_filters)) {
                                    $product_filters[] = $new_filter_id;
                                }

                                $filters[$head['column_name'].'>'.$search_filter] = array(
                                    'filter_id'        => $new_filter_id,
                                    'language_id'      => $head['language_id'],
                                    'filter_group_id'  => $head['filter_group_id'],
                                    'sort_order'       => '0',
                                    'group'            => $head['column_name']
                                );
                            }
                        }
                    }

                    $this->model_tool_synccells->editProductFilters($product_id, $head['filter_group_id'], $product_filters);
                    
                    // end of get filters from synccells
                } //end Product Filters
				
				///////// Push Product Related ///////////////	
				else if ( $head['full_field_name'] == 'product_related.index' ) {
                    $items_separator = isset($head['multiple_items_separator']) ? trim($head['multiple_items_separator']) : ",";
                    
                    //$value = htmlspecialchars($head['data'][$i], ENT_COMPAT, 'UTF-8');
                    $value = $head['data'][$i]; //do not do htmlspecialchars for getProduct already encodes
                    
                    $parts = $this->trimArrayElements(explode($items_separator, $value));
                    
                    $related_product_ids = array();
                    
                    foreach ($parts as $search_related) {
                        //get product id
                        $related_product =  $this->model_tool_synccells->getProduct( $search_related, $index_field );
                        if (!empty($related_product)) {
                            $related_product_ids[] = $related_product['product_id'];
                        }
                    }
                    
                    $this->model_tool_synccells->editProductRelated( $product_id, $related_product_ids );
				}
				
				////////// Push Special Price ////////////////
				else if ( $head['full_field_name'] == 'product_special.price' ) {
				
					if ( !isset($product_specials) ) $product_specials = array();
					
					$product_specials[] = array(
						'price' 			=> $head['data'][$i],
						'customer_group_id'	=> $head['customer_group_id'],
						'priority'			=> $head['priority'],
						'date_start'		=> $head['date_start'],
						'date_end'			=> $head['date_end']
					);
				} //////////////end Special Price
				
				////////// Push Special Prices ////////////////
				else if ( $head['full_field_name'] == 'special.prices' ) {
					
					if ( !isset($product_specials) ) $product_specials = array();
					
					$specials = json_decode($head['data'][$i]);
					
					if ($specials) {
						foreach ($specials as $special) {
							$product_specials[] = array(
								'price' 			=> $special['2'],
								'customer_group_id'	=> $special['0'],
								'priority'			=> $special['1'],
								'date_start'		=> $special['3'],
								'date_end'			=> $special['4']
							);
						}
					}
					
					
				} //////////////end Special Price
				
				////////// Push Discount ////////////////
				else if ( $head['full_field_name'] == 'product_discount.price' ) {
					
					if ( !isset($product_discounts) ) $product_discounts = array();
					
					$product_discounts[] = array(
						'price' 			=> $head['data'][$i],
						'customer_group_id'	=> $head['customer_group_id'],
						'quantity'	        => $head['quantity'],
						'priority'			=> $head['priority'],
						'date_start'		=> $head['date_start'],
						'date_end'			=> $head['date_end']
					);
				} //////////////end Discount
				
				////////// Push Discounts ////////////////
				else if ( $head['full_field_name'] == 'discount.prices' ) {
					
					if ( !isset($product_discounts) ) $product_discounts = array();
					
					$discounts = json_decode($head['data'][$i]);
					
					if ($discounts) {
						foreach ($discounts as $discount) {
							$product_discounts[] = array(
								'price' 			=> $discount['3'],
								'customer_group_id'	=> $discount['0'],
								'quantity'	        => $discount['1'],
								'priority'			=> $discount['2'],
								'date_start'		=> $discount['4'],
								'date_end'			=> $discount['5']
							);
						}
					}
				} //////////////end Discount
				
				////////// Push SEO Keyword ////////////////
				else if ( $head['full_field_name'] == 'url_alias.keyword' ) {
					
					$product_seo_keyword = $head['data'][$i];
					$product_seo_keyword_head = 1;
                
				} //////////////end SEO Keyword
				
				else if ( $head['full_field_name'] == 'product_image.image' ) {
                    
                    $value = $head['data'][$i];
                    $value = trim($value);
                    if ( !empty( $value ) ) {
                        $images = $this->trimArrayElements( explode( trim( $head['listSeparator'] ), $head['data'][$i]) );
                    } else {
                        $images = array();
                    }
                    
                    $this->model_tool_synccells->editProductImages( $product_id, $images );
				} //////////////end Product Images
				
				else if ( $head['full_field_name'] == 'product_to_store.store' ) {
                    
                    $value = $head['data'][$i];
                    $value = trim($value);
                    //separator defaults to comma
                    $separator = (isset($head['multiple_items_separator'])) ? $head['multiple_items_separator'] : ",";
                    if ( !empty( $value ) || $value == '0' ) {
                        $stores = $this->trimArrayElements( explode( trim( $separator ), $head['data'][$i]) );
                    } else {
                        $stores = array();
                    }
                    
                    $this->model_tool_synccells->editProductStores( $product_id, $stores );
                }
				
				//////// Attributes ///////////////////////
				else if ( $head['full_field_name'] == 'product_attribute.text' ) {
                    
                    if ( !isset($head['language_id']) ) 
						return array( "error" => "language_id for column " . $head['column_name'] . " is not set");
					
					$value = htmlspecialchars($head['data'][$i], ENT_COMPAT, 'UTF-8');
					
					if ($head['language_id'] == 0) {
                        //add same attribute for all languages
                        foreach ($languages as $language_id) {
                            $this->model_tool_synccells->editProductAttribute($product_id, $head['attribute_id'], $language_id, $value);
                        }
                    } else {
                        //add single attribute line
                        $this->model_tool_synccells->editProductAttribute($product_id, $head['attribute_id'], $head['language_id'], $value);
                    }
                } //end Attributes
                
                //=== push insert custom field actions ===
                //e.g. else if ( $head['full_field_name'] == 'custom_field_name' ) { ...your actions here... }
                
                //=== end push insert custom field actions ===
                
			} //end foreach header
			
			if ( !empty($product_operations) ) {
				$this->model_tool_synccells->editProduct($product_operations, $product_id);
			}
			
			if ( !empty($product_description_operations) ) {
				$this->model_tool_synccells->editProductDescription($product_description_operations, $product_id);
			}

            if (isset($product_specials)) $this->model_tool_synccells->editProductSpecials($product_id, $product_specials);
			
			
			if (isset($product_discounts)) $this->model_tool_synccells->editProductDiscounts($product_id, $product_discounts);
			
			
			if ($product_seo_keyword_head) {
				$this->model_tool_synccells->editProductSeoKeyword($product_id, $product_seo_keyword);
			}
			
			if ( !empty($synccells_options) && (isset($sheet_data['optionFields'])) && ($key_option_value !== '') ) {
				$product_options = array();
				// get options and option values from synccells
				foreach ($synccells_options[$i] as $synccells_option_row) {
					$current_option_name = '';
					$current_option_type = '';

					if ($key_option_option !== '') {
						$current_option_name = $synccells_option_row[$key_option_option];
					} else {
						$current_option_name = $synccells_default_settings['synccells_option_description'][$synccells_option_language]['name'];
					}
				
					if ($key_option_type !== '') {
						$current_option_type = $synccells_option_row[$key_option_type];
					} else {
						$current_option_type = $synccells_default_settings['synccells_option_type'];
					}
				
				if (!empty($current_option_name)/* && ($key_option_value !== '')*/) {
					if (isset($options[$current_option_name])) {
						// if option exist in opencart
						
						$new_option_id = $options[$current_option_name]['option_id'];
						
						if (!isset($product_options[$new_option_id])) {
							$product_options[$new_option_id] = array(
								'option_id'            => $new_option_id,
								'type'                 => $current_option_type,
								'value'                => $synccells_option_row[$key_option_value],
								'required'             => ($key_option_required !== '') ? $synccells_option_row[$key_option_required] : $synccells_default_settings['synccells_option_required'],
								'product_option_value' => array()
							);	
						} else {
						
						}
						
						if (!isset($options[$current_option_name]['option_values'][$synccells_option_row[$key_option_value]])) {
							// if option value exist doesn't exist in opencart
							// creating new option value
							$new_option_value_id = $this->model_tool_synccells->addOptionValue($new_option_id, $synccells_option_row[$key_option_value]);
							// end of create new option value
							
							$options[$current_option_name]['option_values'] += array(
								$synccells_option_row[$key_option_value] => array(
									'option_value_id' => $new_option_value_id,
									'image'           => ''
								)
							);
							
						} else {
							$new_option_value_id = $options[$current_option_name]['option_values'][$synccells_option_row[$key_option_value]]['option_value_id'];
						}
						
							$product_options[$new_option_id]['product_option_value'][] = array(
								'option_value_id'      => $new_option_value_id,
								'quantity'             => ($key_option_quantity !== '') ? $synccells_option_row[$key_option_quantity] : $synccells_default_settings['synccells_option_quantity'],
								'subtract'             => ($key_option_subtract !== '') ? $synccells_option_row[$key_option_subtract] : $synccells_default_settings['synccells_option_subtract'],
								'price_prefix'         => ($key_option_price !== '') ? (($synccells_option_row[$key_option_price] < 0) ? '-' : '+') : $synccells_default_settings['synccells_option_price_prefix'],
								'price'                => ($key_option_price !== '') ? ltrim($synccells_option_row[$key_option_price], '-') : $synccells_default_settings['synccells_option_price'],
								'points_prefix'        => ($key_option_points !== '') ? (($synccells_option_row[$key_option_points] < 0) ? '-' : '+') : $synccells_default_settings['synccells_option_points_prefix'],
								'points'               => ($key_option_points !== '') ? ltrim($synccells_option_row[$key_option_points], '-') : $synccells_default_settings['synccells_option_points'],
								'weight_prefix'        => ($key_option_weight !== '') ? (($synccells_option_row[$key_option_weight] < 0) ? '-' : '+') : $synccells_default_settings['synccells_option_weight_prefix'],						
								'weight'               => ($key_option_weight !== '') ? ltrim($synccells_option_row[$key_option_weight], '-') : $synccells_default_settings['synccells_option_weight']
							);	
					} else {
						// create new option
						$new_option_id = $this->model_tool_synccells->addOption($current_option_name, $current_option_type);
						
						if (!isset($product_options[$new_option_id])) {
							$product_options[$new_option_id] = array(
								'option_id'            => $new_option_id,
								'type'                 => $current_option_type,
								'value'                => $synccells_option_row[$key_option_value],
								'required'             => ($key_option_required !== '') ? $synccells_option_row[$key_option_required] : $synccells_default_settings['synccells_option_required'],
								'product_option_value' => array()
							);	
						}


//						if (!isset($options[$current_option_name]['option_values'][$synccells_option_row[$key_option_value]])) {
							// creating new option value
							$new_option_value_id = $this->model_tool_synccells->addOptionValue($new_option_id, $synccells_option_row[$key_option_value]);
							// end of create new option value
//						}	

						$product_options[$new_option_id]['product_option_value'][] = array(
							'option_value_id'      => $new_option_value_id,
							'quantity'             => ($key_option_quantity !== '') ? $synccells_option_row[$key_option_quantity] : $synccells_default_settings['synccells_option_quantity'],
							'subtract'             => ($key_option_subtract !== '') ? $synccells_option_row[$key_option_subtract] : $synccells_default_settings['synccells_option_subtract'],
							'price_prefix'         => ($key_option_price !== '') ? (($synccells_option_row[$key_option_price] < 0) ? '-' : '+') : $synccells_default_settings['synccells_option_price_prefix'],
							'price'                => ($key_option_price !== '') ? ltrim($synccells_option_row[$key_option_price], '-') : $synccells_default_settings['synccells_option_price'],
							'points_prefix'        => ($key_option_points !== '') ? (($synccells_option_row[$key_option_points] < 0) ? '-' : '+') : $synccells_default_settings['synccells_option_points_prefix'],
							'points'               => ($key_option_points !== '') ? ltrim($synccells_option_row[$key_option_points], '-') : $synccells_default_settings['synccells_option_points'],
							'weight_prefix'        => ($key_option_weight !== '') ? (($synccells_option_row[$key_option_weight] < 0) ? '-' : '+') : $synccells_default_settings['synccells_option_weight_prefix'],						
							'weight'               => ($key_option_weight !== '') ? ltrim($synccells_option_row[$key_option_weight], '-') : $synccells_default_settings['synccells_option_weight']
						);	

						$options[$current_option_name] = array(
							'option_id'    => $new_option_id,
							'type'         => $current_option_type,
							'sort_order'   => '0',
							'option_values' => array(
								$synccells_option_row[$key_option_value] => array(
									'option_value_id' => $new_option_value_id,
									'image'           => ''
								)
							)						
						);
					}
					}
				}

				$this->model_tool_synccells->editProductOptions($product_id, $product_options);

				
			} // end of get options and option values from synccells
			
            //=== push insert custom row actions ===
            
            //=== end push insert custom row actions ===
			
		} //end foreach product rows
        
        $this->cache->delete('product');
        $this->cache->delete('category');
        $this->cache->delete('manufacturer');
		
		$result['ids'] = $ids;
		$result['columns'] = $columns;
		
		return $result;
	}
	
	private function trimArrayElements($parts) {
		//Trimming whitespace around parts
		$clean_parts = array();
		
		foreach ( $parts as $part ) {
			$part = trim($part);
			$part = trim($part,chr(0xC2).chr(0xA0)); //removes &nbsp;
			$clean_parts[] = $part;
		}
		//end trimmming
		return $clean_parts;
	}
	
    private function getImageList( $path = false ){
        
        if(!$path){
                $dir = DIR_IMAGE;
        }else{
            if(is_dir(DIR_IMAGE.$path)){
                $dir = DIR_IMAGE.$path;
            }else{
                return array( 'error' => DIR_IMAGE . $path . ' : Path not found on the server, please verify the path and try again');
            }
        }
        //return array("test" => is_dir($dir));
        
        $results = array();
        
        if (is_dir($dir)) {
            
            $iterator = new RecursiveDirectoryIterator($dir);
            
            foreach ( new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::CHILD_FIRST) as $file ) {
                
                if ( $file->isFile() ) {
                    $thispath = str_replace('\\', '/', $file);
                    $thisfile = utf8_encode($file->getFilename());
                    $results[] = str_replace(DIR_IMAGE,'',$thispath);
                }
            }
        }
        
        return array('imagelist' => $results); 
    }
	
}

