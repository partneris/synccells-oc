<?php
// Heading Goes here:
$_['heading_title']    = 'Ⓟ SyncCells';


// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module SyncCells!';
$_['text_module_version'] = 'Extension version';
$_['text_edit']           = 'Edit';
$_['text_choose']         = 'Choose';
$_['text_select']         = 'Select';
$_['text_radio']          = 'Radio';
$_['text_checkbox']       = 'Checkbox';
$_['text_image']          = 'Image';
$_['text_input']          = 'Input';
$_['text_text']           = 'Text';
$_['text_textarea']       = 'Textarea';
$_['text_file']           = 'File';
$_['text_date']           = 'Date';
$_['text_datetime']       = 'Date &amp; Time';
$_['text_time']           = 'Time';

$_['tab_product_values']  = 'Product Default Values';
$_['tab_category_values'] = 'Category Default Values';
$_['tab_option_values']   = 'Option Default Values';

// Entry
$_['entry_secret']              = 'Secret:';
$_['entry_tax_class']           = 'Product Tax Class:';
$_['entry_quantity']            = 'Product Quantity';
$_['entry_minimum_quantity']    = 'Product Minimum Quantity:';
$_['entry_subtract_stock']      = 'Product Subtract Stock:';
$_['entry_out_of_stock_status'] = 'Product Out Of Stock Status:';
$_['entry_requires_shipping']   = 'Product Requires Shipping:';
$_['entry_length_class']        = 'Product Length Class:';
$_['entry_weight_class']        = 'Product Weight Class:';
$_['entry_store']               = 'Product To Store:';
$_['entry_product_status']      = 'Product Status:';
$_['entry_categories_top']      = 'Add Categories to Top Menu:';
$_['entry_option_name']         = 'Option Name';
$_['entry_option_type']         = 'Option Type';
$_['entry_option_required']     = 'Product Option Required';
$_['entry_option_quantity']     = 'Product Option Quantity';
$_['entry_option_subtract']     = 'Product Option Subtract Stock';
$_['entry_option_price']        = 'Product Option Price';
$_['entry_option_points']       = 'Product Option Points';
$_['entry_option_weight']       = 'Product Option Weight';

$_['help_minimum']           = 'Force a minimum ordered amount';
$_['help_stock_status']      = 'Status shown when a product is out of stock';
$_['help_top']               = 'Display in the top menu bar. Only works for the top parent categories.';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify module SyncCells!';
?>