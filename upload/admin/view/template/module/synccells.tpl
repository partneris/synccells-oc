<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-synccells" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-synccells" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            <li><a href="#tab-product-values" data-toggle="tab"><?php echo $tab_product_values; ?></a></li>
            <li><a href="#tab-category-values" data-toggle="tab"><?php echo $tab_category_values; ?></a></li>
            <li><a href="#tab-option-values" data-toggle="tab"><?php echo $tab_option_values; ?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
		  <div class="form-group">
			<label class="col-sm-2 control-label"><?php echo $text_module_version; ?> <?php echo SYNCCELLS_VERSION;?></label>
		  </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-secret"><?php echo $entry_secret; ?></label>
            <div class="col-sm-10">
              <input type="text" name="synccells_secret" value="<?php echo $synccells_secret; ?>" placeholder="<?php echo $entry_secret; ?>" id="input-secret" class="form-control" />
            </div>
          </div>
            </div>
            <div class="tab-pane" id="tab-product-values">
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="input-tax-class"><?php echo $entry_tax_class; ?></label>
			<div class="col-sm-10">
			  <select name="synccells_tax_class_id" id="input-tax-class" class="form-control">
				<option value="0"><?php echo $text_none; ?></option>
				<?php foreach ($tax_classes as $tax_class) { ?>
				<?php if ($tax_class['tax_class_id'] == $synccells_tax_class_id) { ?>
				<option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
				<?php } else { ?>
				<option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
				<?php } ?>
				<?php } ?>
			  </select>
			</div>
		  </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-quantity"><?php echo $entry_quantity; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="synccells_quantity" value="<?php echo $synccells_quantity; ?>" placeholder="<?php echo $entry_quantity; ?>" id="input-quantity" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-minimum"><span data-toggle="tooltip" title="<?php echo $help_minimum; ?>"><?php echo $entry_minimum_quantity; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="synccells_minimum" value="<?php echo $synccells_minimum; ?>" placeholder="<?php echo $entry_minimum_quantity; ?>" id="input-minimum" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-subtract"><?php echo $entry_subtract_stock; ?></label>
                <div class="col-sm-10">
                  <select name="synccells_subtract" id="input-subtract" class="form-control">
                    <?php if ($synccells_subtract) { ?>
                    <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                    <option value="0"><?php echo $text_no; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_yes; ?></option>
                    <option value="0" selected="selected"><?php echo $text_no; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-stock-status"><span data-toggle="tooltip" title="<?php echo $help_stock_status; ?>"><?php echo $entry_out_of_stock_status; ?></span></label>
                <div class="col-sm-10">
                  <select name="synccells_stock_status_id" id="input-stock-status" class="form-control">
                    <?php foreach ($stock_statuses as $stock_status) { ?>
                    <?php if ($stock_status['stock_status_id'] == $synccells_stock_status_id) { ?>
                    <option value="<?php echo $stock_status['stock_status_id']; ?>" selected="selected"><?php echo $stock_status['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $stock_status['stock_status_id']; ?>"><?php echo $stock_status['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $entry_requires_shipping; ?></label>
                <div class="col-sm-10">
                  <label class="radio-inline">
                    <?php if ($synccells_shipping) { ?>
                    <input type="radio" name="synccells_shipping" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <?php } else { ?>
                    <input type="radio" name="synccells_shipping" value="1" />
                    <?php echo $text_yes; ?>
                    <?php } ?>
                  </label>
                  <label class="radio-inline">
                    <?php if (!$synccells_shipping) { ?>
                    <input type="radio" name="synccells_shipping" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                    <?php } else { ?>
                    <input type="radio" name="synccells_shipping" value="0" />
                    <?php echo $text_no; ?>
                    <?php } ?>
                  </label>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-length-class"><?php echo $entry_length_class; ?></label>
                <div class="col-sm-10">
                  <select name="synccells_length_class_id" id="input-length-class" class="form-control">
                    <?php foreach ($length_classes as $length_class) { ?>
                    <?php if ($length_class['length_class_id'] == $synccells_length_class_id) { ?>
                    <option value="<?php echo $length_class['length_class_id']; ?>" selected="selected"><?php echo $length_class['title']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $length_class['length_class_id']; ?>"><?php echo $length_class['title']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-weight-class"><?php echo $entry_weight_class; ?></label>
                <div class="col-sm-10">
                  <select name="synccells_weight_class_id" id="input-weight-class" class="form-control">
                    <?php foreach ($weight_classes as $weight_class) { ?>
                    <?php if ($weight_class['weight_class_id'] == $synccells_weight_class_id) { ?>
                    <option value="<?php echo $weight_class['weight_class_id']; ?>" selected="selected"><?php echo $weight_class['title']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $weight_class['weight_class_id']; ?>"><?php echo $weight_class['title']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $entry_store; ?></label>
                <div class="col-sm-10">
                  <div class="well well-sm" style="height: 150px; overflow: auto;">
                    <div class="checkbox">
                      <label>
                        <?php if (in_array(0, $synccells_product_store)) { ?>
                        <input type="checkbox" name="synccells_product_store[]" value="0" checked="checked" />
                        <?php echo $text_default; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="synccells_product_store[]" value="0" />
                        <?php echo $text_default; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php foreach ($stores as $store) { ?>
                    <div class="checkbox">
                      <label>
                        <?php if (in_array($store['store_id'], $synccells_product_store)) { ?>
                        <input type="checkbox" name="synccells_product_store[]" value="<?php echo $store['store_id']; ?>" checked="checked" />
                        <?php echo $store['name']; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="synccells_product_store[]" value="<?php echo $store['store_id']; ?>" />
                        <?php echo $store['name']; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-product-status"><?php echo $entry_product_status; ?></label>
                <div class="col-sm-10">
                  <select name="synccells_product_status" id="input-product-status" class="form-control">
                    <?php if ($synccells_product_status) { ?>
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <option value="0"><?php echo $text_disabled; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tab-category-values">
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-top"><span data-toggle="tooltip" title="<?php echo $help_top; ?>"><?php echo $entry_categories_top; ?></span></label>
                <div class="col-sm-10">
                  <div class="checkbox">
                    <label>
                      <?php if ($synccells_top) { ?>
                      <input type="checkbox" name="synccells_top" value="1" checked="checked" id="input-top" />
                      <?php } else { ?>
                      <input type="checkbox" name="synccells_top" value="1" id="input-top" />
                      <?php } ?>
                      &nbsp; </label>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tab-option-values">
	          <div class="form-group">
	            <label class="col-sm-2 control-label"><?php echo $entry_option_name; ?></label>
	            <div class="col-sm-10">
	              <?php foreach ($languages as $language) { ?>
	<?php if (version_compare(VERSION, '2.2.0.0', '>=')) {
//		$flag_url = HTTP_CATALOG . 'catalog/language/' . $language['code'] . '/' . $language['image'];
		$flag_url = 'language/' . $language['code'] . '/' . $language['code'] . '.png';
	} else {
		$flag_url = 'view/image/flags/' . $language['image'];
	} ?>
	              <div class="input-group"><span class="input-group-addon"><img src="<?php echo $flag_url; ?>" title="<?php echo $language['name']; ?>" /></span>
	                <input type="text" name="synccells_option_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($synccells_option_description[$language['language_id']]) ? $synccells_option_description[$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_option_name; ?>" class="form-control" />
	              </div>
	              <?php } ?>
	            </div>
	          </div>
	          <div class="form-group">
	            <label class="col-sm-2 control-label" for="input-option-type"><?php echo $entry_option_type; ?></label>
	            <div class="col-sm-10">
	              <select name="synccells_option_type" id="input-option-type" class="form-control">
	                <optgroup label="<?php echo $text_choose; ?>">
	                <?php if ($synccells_option_type == 'select') { ?>
	                <option value="select" selected="selected"><?php echo $text_select; ?></option>
	                <?php } else { ?>
	                <option value="select"><?php echo $text_select; ?></option>
	                <?php } ?>
	                <?php if ($synccells_option_type == 'radio') { ?>
	                <option value="radio" selected="selected"><?php echo $text_radio; ?></option>
	                <?php } else { ?>
	                <option value="radio"><?php echo $text_radio; ?></option>
	                <?php } ?>
	                <?php if ($synccells_option_type == 'checkbox') { ?>
	                <option value="checkbox" selected="selected"><?php echo $text_checkbox; ?></option>
	                <?php } else { ?>
	                <option value="checkbox"><?php echo $text_checkbox; ?></option>
	                <?php } ?>
	                <?php if ($synccells_option_type == 'image') { ?>
	                <option value="image" selected="selected"><?php echo $text_image; ?></option>
	                <?php } else { ?>
	                <option value="image"><?php echo $text_image; ?></option>
	                <?php } ?>
	                </optgroup>
	                <optgroup label="<?php echo $text_input; ?>">
	                <?php if ($synccells_option_type == 'text') { ?>
	                <option value="text" selected="selected"><?php echo $text_text; ?></option>
	                <?php } else { ?>
	                <option value="text"><?php echo $text_text; ?></option>
	                <?php } ?>
	                <?php if ($synccells_option_type == 'textarea') { ?>
	                <option value="textarea" selected="selected"><?php echo $text_textarea; ?></option>
	                <?php } else { ?>
	                <option value="textarea"><?php echo $text_textarea; ?></option>
	                <?php } ?>
	                </optgroup>
	                <optgroup label="<?php echo $text_file; ?>">
	                <?php if ($synccells_option_type == 'file') { ?>
	                <option value="file" selected="selected"><?php echo $text_file; ?></option>
	                <?php } else { ?>
	                <option value="file"><?php echo $text_file; ?></option>
	                <?php } ?>
	                </optgroup>
	                <optgroup label="<?php echo $text_date; ?>">
	                <?php if ($synccells_option_type == 'date') { ?>
	                <option value="date" selected="selected"><?php echo $text_date; ?></option>
	                <?php } else { ?>
	                <option value="date"><?php echo $text_date; ?></option>
	                <?php } ?>
	                <?php if ($synccells_option_type == 'time') { ?>
	                <option value="time" selected="selected"><?php echo $text_time; ?></option>
	                <?php } else { ?>
	                <option value="time"><?php echo $text_time; ?></option>
	                <?php } ?>
	                <?php if ($synccells_option_type == 'datetime') { ?>
	                <option value="datetime" selected="selected"><?php echo $text_datetime; ?></option>
	                <?php } else { ?>
	                <option value="datetime"><?php echo $text_datetime; ?></option>
	                <?php } ?>
	                </optgroup>
	              </select>
	            </div>
	          </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-option-required"><?php echo $entry_option_required; ?></label>
				<div class="col-sm-10">
				  <select name="synccells_option_required" id="input-option-required" class="form-control">
					<?php if ($synccells_option_required) { ?>
					<option value="1" selected="selected"><?php echo $text_yes; ?></option>
					<option value="0"><?php echo $text_no; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_yes; ?></option>
					<option value="0" selected="selected"><?php echo $text_no; ?></option>
					<?php } ?>
				  </select>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-option-quantity"><?php echo $entry_option_quantity; ?></label>
				<div class="col-sm-10">
				  <input type="text" name="synccells_option_quantity" value="<?php echo $synccells_option_quantity; ?>" placeholder="<?php echo $entry_option_quantity; ?>" class="form-control" />
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-option-subtract"><?php echo $entry_option_subtract; ?></label>
				<div class="col-sm-10">
				  <select name="synccells_option_subtract" id="input-option-subtract" class="form-control">
					<?php if ($synccells_option_subtract) { ?>
					<option value="1" selected="selected"><?php echo $text_yes; ?></option>
					<option value="0"><?php echo $text_no; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_yes; ?></option>
					<option value="0" selected="selected"><?php echo $text_no; ?></option>
					<?php } ?>
				  </select>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-option-price"><?php echo $entry_option_price; ?></label>
				<div class="col-sm-10">
				  <select name="synccells_option_price_prefix" class="form-control">
					<?php if ($synccells_option_price_prefix == '+') { ?>
					<option value="+" selected="selected">+</option>
					<?php } else { ?>
					<option value="+">+</option>
					<?php } ?>
					<?php if ($synccells_option_price_prefix == '-') { ?>
					<option value="-" selected="selected">-</option>
					<?php } else { ?>
					<option value="-">-</option>
					<?php } ?>
				  </select>
				  <input type="text" name="synccells_option_price" value="<?php echo $synccells_option_price; ?>" placeholder="<?php echo $entry_option_price; ?>" class="form-control" />
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-option-points"><?php echo $entry_option_points; ?></label>
				<div class="col-sm-10">
				  <select name="synccells_option_points_prefix" class="form-control">
				    <?php if ($synccells_option_points_prefix == '+') { ?>
				    <option value="+" selected="selected">+</option>
				    <?php } else { ?>
				    <option value="+">+</option>
				    <?php } ?>
				    <?php if ($synccells_option_points_prefix == '-') { ?>
				    <option value="-" selected="selected">-</option>
				    <?php } else { ?>
				    <option value="-">-</option>
				    <?php } ?>
				  </select>
				  <input type="text" name="synccells_option_points" value="<?php echo $synccells_option_points; ?>" placeholder="<?php echo $entry_option_points; ?>" class="form-control" />
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-option-weight"><?php echo $entry_option_weight; ?></label>
				<div class="col-sm-10">
				  <select name="synccells_option_weight_prefix" class="form-control">
				    <?php if ($synccells_option_weight_prefix == '+') { ?>
				    <option value="+" selected="selected">+</option>
				    <?php } else { ?>
				    <option value="+">+</option>
				    <?php } ?>
				    <?php if ($synccells_option_weight_prefix == '-') { ?>
				    <option value="-" selected="selected">-</option>
				    <?php } else { ?>
				    <option value="-">-</option>
				    <?php } ?>
				  </select>
				  <input type="text" name="synccells_option_weight" value="<?php echo $synccells_option_weight; ?>" placeholder="<?php echo $entry_option_weight; ?>" class="form-control" />
				</div>
			  </div>

            </div>
          </div>
		  
		  
        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?> 